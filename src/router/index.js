import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import File from '../views/file/File.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { path: '/file', query: { filepath: '/', filetype: 0 } }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { title: '登录' },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { title: '注册' }
  },
  {
    path: '/file',
    name: 'File',
    component: File,
    meta: {
      requireAuth: true,
      title: '欧拉网盘',
      content: {
        description: '基于springboot + vue 框架开发的Web文件系统，旨在为用户提供一个简单、方便的文件存储方案'
      }
    }
  },
  {
    path: '/500',
    name: 'Error_500',
    component: () => import(/* webpackChunkName: "error_500" */ '@/views/ErrorPage/500.vue'),
    meta: { title: '500 - 网盘' },
  }, {
    path: '/401',
    name: 'Error_401',
    component: () => import(/* webpackChunkName: "error_401" */ '@/views/ErrorPage/401.vue'),
    meta: { title: '401 - 网盘' },
  }, {
    path: '*',
    name: 'Error_404',
    component: () => import(/* webpackChunkName: "error_404" */ '@/views/ErrorPage/404.vue'),
    meta: { title: '404 - 网盘' },
  }
  
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
