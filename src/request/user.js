import { get,post } from './http'


export const sendCaptcha = p => post('/user/email', p, true)//发送验证码
export const login = p => post('/user/login', p, true); //登录 
export const checkUserLoginInfo = p => get('/user/userlogininfo', p); //登录状态及个人信息
export const logout = p => post('/user/logout', p, true); //退出登录
export const upuserinfo = p => post('/user/upinfo', p) //设置用戶信息
export const register = p => post('/user/register', p, true); //注册 