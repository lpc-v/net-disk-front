//  文件模块接口
import { get,post,jsonPost } from './http'
import {BASE_URL} from './base.js'
import axios from "axios"

export const getfilelist = p => get('/file/list', p); //获取当前目录下文件列表 参数为当前目录  ok
export const getstorage = p => get('/file/storage', p); //获取存储占用  ok
export const unzipfile = p => post('/file/unzipfile', p); //解压文件  useless
export const deleteFile = p => post('/file/delete', p, true); //删除文件  ok
export const selectFileByFileType = p => get('/file/listbytype', p); // 通过文件类型选择文件  ok
export const getFileTree = p => get('/file/tree', p); //获取文件的树结构  todo
export const moveFile = p => post('/file/move', p, true); //移动文件  todo
export const createFile = p => post('/file/createdir', p, true); //创建文件  todo
export const batchDeleteFile = p => jsonPost('/file/batchdeletefile', p); //批量删除文件  todo
export const batchMoveFile = p => post('/file/batchmovefile', p); //批量移动文件  todo
export const searchFile = p => get('/file/search', p);


export const uploadUrl = 'http://mockjs.docway.net/mock/1WklN4SWSjh/upload' //文件上传路径
export const downloadUrl = BASE_URL + '/file/download/'

export const download = row => {
    if (row.isdir)
        return
    let data = {
        filename: row.filename,
        extendname: row.extendname,
        filepath: row.filepath
    }
    console.log("下载：" + data.filename)
    let url = downloadUrl + '?filename=' + data.filename + '&extendname=' + data.extendname + '&filepath=' + data.filepath
    window.open(url)
}