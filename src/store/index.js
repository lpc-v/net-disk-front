import Vue from 'vue'
import Vuex from 'vuex'
import { checkUserLoginInfo } from '../request/user';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogin: sessionStorage.getItem('isLogin'), //初始时候给一个 isLogin = false 表示用户未登录
    username: "",
    nickname: '',
    userId: 0,
    userImgUrl: "",
    userInfoObj: {},
    storageInfoObject: "",
    operaColumnExpand: sessionStorage.getItem("operaColumnExpand"), //  操作列是否展开，0不展开，1展开
    isFolder: sessionStorage.getItem("isFolder"), //  左侧栏是否折叠，0不折叠，1折叠
    selectedColumnList: sessionStorage.getItem("selectedColumnList"), //  列显隐
    imageModel: sessionStorage.getItem("imageModel"), //  图片类型页面是否展示为网格模式，0不是，1是
  },
  getters: {
    isLogin: (state) => state.isLogin,
    username: (state) => state.username,
    nickname: (state) => state.nickname,
    userId: (state) => state.userId,
    userImgUrl: (state) => state.userImgUrl,
    userInfoObj: (state) => state.userInfoObj,
    storageInfoObject: (state) => state.storageInfoObject,
    operaColumnExpand: (state) =>
    state.operaColumnExpand !== null
      ? Number(state.operaColumnExpand)
      : document.body.clientWidth > 1280
        ? 1
        : 0, //  操作列是否展开，0不展开，1展开
    isFolder: (state) => Number(state.isFolder), //  左侧栏是否折叠，0不折叠，1折叠
    selectedColumnList: (state) =>
      state.selectedColumnList
        ? state.selectedColumnList.split(",")
        : ["extendname", "filesize", "uploadtime"], //  列显隐
    imageModel: (state) => Number(state.imageModel), //  图片类型页面是否展示为网格模式，0不是，1是
  },
  mutations: {
    changeLogin(state, data) {
      state.isLogin = data;
    },
    changeUsername(state, data) {
      state.username = data;
    },
    changeNickname(state, data) {
      state.nickname = data
    },
    changeUserId(state, data) {
      state.userId = data;
    },
    changeUserImgUrl(state, data) {
      state.userImgUrl = data;
    },
    changeUserInfoObj(state, data) {
      state.userInfoObj = Object.assign({}, state.userInfoObj, data);
    },
    changeStorageInfoObj(state, data) {
      state.storageInfoObject = data
    },
    changeOperaColumnExpand(state, data) {
      sessionStorage.setItem("operaColumnExpand", data);
      state.operaColumnExpand = data;
    },
    changeIsFolder(state, data) {
      sessionStorage.setItem("isFolder", data);
      state.isFolder = data;
    },
    changeSelectedColumnList(state, data) {
      sessionStorage.setItem("selectedColumnList", data.toString());
      state.selectedColumnList = data.toString();
    },
    changeImageModel(state, data) {
      sessionStorage.setItem("imageModel", data);
      state.imageModel = data;
    },
  },
  actions: {
    setUserInfo(context) {
      checkUserLoginInfo().then( res => {
        if (res.code == 200) {
          sessionStorage.setItem('isLogin', true)
          context.commit("changeLogin", true)
          context.commit('changeNickname', res.data.nickname)
          context.commit('changeUserId', res.data.userId)
          context.commit('changeUsername', res.data.username)
          context.commit('changeUserImgUrl', res.data.userImgUrl)
          context.commit('changeUserInfoObj', res.data.userInfoObj)
        }else{
          context.commit("changeLogin", false)
        }
      })
    },
    clearAll(context) {
      context.commit("changeLogin", false)
      context.commit("changeNickname", '')
      context.commit('changeUserId', '')
      context.commit('changeUsername', '')
      context.commit('changeUserImgUrl', '')
      context.commit('changeUserInfoObj', '')
    }
  },
  modules: {
  }
})
